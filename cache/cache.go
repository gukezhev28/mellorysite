package cache

import (
	"errors"
	"sync"
	"time"
)

var Users = make(map[string]UserSet)

type User struct {
	UserID    int    `json:"user_id"`
	Name      string `json:"name"`
	Login     string `json:"login"`
	Password  string `json:"password"`
	Privilege string `json:"privilege"`
}

// UserSet struct for cache map
type UserSet struct {
	Value   User
	Created time.Time
}

type UserCache struct {
	sync.RWMutex
	Items map[string]UserSet
}

func NewCacheUser() *UserCache {
	return &UserCache{
		Items: Users,
	}
}

func (c *UserCache) SetCacheUser(key string, value User) {
	c.Lock()
	defer c.Unlock()

	c.Items[key] = UserSet{
		Value:   value,
		Created: time.Now(),
	}
}

func (c *UserCache) GetCacheUser(key string) (*UserSet, bool) {
	c.RLock()
	defer c.RUnlock()

	item, found := c.Items[key]
	if !found {
		return nil, false
	}

	return &item, true
}

func (c *UserCache) DeleteCacheUser(key string) error {
	c.Lock()
	defer c.Unlock()

	if _, found := c.Items[key]; !found {
		return errors.New("Item not found")
	}

	delete(c.Items, key)
	return nil
}
