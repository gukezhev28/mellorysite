package config

import "mellorysite/cache"

const (
	// SecretKey - secret token for JWT creation
	SecretKey = "frogs-are-the-best"
)

// AdminAccount inits the admin accounts
var AdminAccount = cache.User{
	UserID:    1,
	Name:      "Sosiska",
	Login:     "admin",
	Password:  "admin",
	Privilege: "superuser",
}
