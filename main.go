package main

import (
	"log"
	"mellorysite/cache"
	"net/http"

	"github.com/gin-gonic/gin"
)

var cacheData = cache.NewCacheUser()

var router *gin.Engine

func main() {

	gin.SetMode(gin.ReleaseMode)

	router = gin.Default()

	router.LoadHTMLGlob("templates/*.html")

	initRoutes()

	log.Fatal(router.Run())
}

func render(c *gin.Context, templateName string, data gin.H) {
	loggedInInterface, _ := c.Get("is_logged_in")
	data["is_logged_in"] = loggedInInterface.(bool)

	c.HTML(http.StatusOK, templateName, data)
}
