package main

import (
	"fmt"
	"mellorysite/config"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// This middleware sets whether the user is logged in or not
func setUserStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		if token, err := c.Cookie("session_token"); err == nil || token != "" {
			c.Set("is_logged_in", true)
		} else {
			c.Set("is_logged_in", false)
		}
	}
}

func ensureLoggedIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		loggedInInterface, _ := c.Get("is_logged_in")
		loggedIn := loggedInInterface.(bool)
		if !loggedIn {
			//if token, err := c.Cookie("session_token"); err != nil || token == "" {
			// c.AbortWithStatus(http.StatusUnauthorized)
			c.Redirect(http.StatusTemporaryRedirect, "/u/login")
		}
	}
}

func ensureNotLoggedIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		loggedInInterface, _ := c.Get("is_logged_in")
		loggedIn := loggedInInterface.(bool)
		if loggedIn {
			// if token, err := c.Cookie("session_token"); err == nil || token != "" {
			// c.AbortWithStatus(http.StatusUnauthorized)
			c.Redirect(http.StatusTemporaryRedirect, "/")
		}
	}
}

func createToken(login, password string) (string, error) {
	atClaims := jwt.MapClaims{}
	atClaims["login"] = login
	atClaims["password"] = password

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(config.SecretKey))
	if err != nil {
		fmt.Println("Token creation error:", err)
		return "", err
	}
	return token, nil
}

func decodeToken(tokenString string) (jwt.MapClaims, error) {
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretKey), nil
	})
	if err != nil {
		fmt.Println("Token decoding error:", err)
		return jwt.MapClaims{}, err
	}

	return claims, nil
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		fmt.Println("Hash creation error:", err)
		return "", err
	}
	return string(bytes), err
}
