package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func initRoutes() {
	createAdmin()

	router.Static("/css", "templates/css")

	router.Use(setUserStatus())

	router.GET("/", showHomePage)

	userRouter := router.Group("/u")
	userRouter.Static("/css", "templates/css")
	{
		userRouter.GET("/", ensureLoggedIn(), showProfile)

		userRouter.GET("/login", ensureNotLoggedIn(), showLoginPage)
		userRouter.POST("/login", ensureNotLoggedIn(), loginUser)

		userRouter.GET("/logged", ensureLoggedIn(), showLoggedInSuccessfully)

		userRouter.GET("/signup", ensureNotLoggedIn(), showSignUpPage)
		userRouter.POST("/signup", ensureNotLoggedIn(), signupUser)

		userRouter.GET("/logout", ensureLoggedIn(), logoutUser)
	}

	goalsRouter := router.Group("/goals")
	goalsRouter.Static("/css", "templates/css")
	{
		goalsRouter.GET("/", ensureLoggedIn(), showGoalsPage)

		goalsRouter.GET("/create", ensureLoggedIn(), showGoalCreationPage)
		goalsRouter.POST("/create", ensureLoggedIn(), createGoal)
	}
}

func showHomePage(c *gin.Context) {
	render(c, "index.html", gin.H{
		"title": "Home Page"})
}

func showProfile(c *gin.Context) {
	render(c, "user-profile.html", gin.H{
		"title": "Profile",
	})
}

func showLoginPage(c *gin.Context) {
	render(c, "login-page.html", gin.H{
		"title": "Log In",
	})
}

func showSignUpPage(c *gin.Context) {
	render(c, "signup-page.html", gin.H{
		"title": "Sign Up",
	})
}

func loginUser(c *gin.Context) {
	login := c.PostForm("login")
	password := c.PostForm("password")

	if isUserValid(login, password) {
		token, err := createToken(login, password)
		if err != nil {
			return
		}

		c.SetCookie("session_token", token, 3600, "/", "localhost", false, true)
		c.Set("is_logged_in", true)

		c.Redirect(http.StatusMovedPermanently, "/u/logged")

	} else {
		render(c, "login-page.html", gin.H{
			"loginError": true,
		})
	}
}

func showLoggedInSuccessfully(c *gin.Context) {
	token, err := c.Cookie("session_token")
	if err != nil {
		fmt.Println("Token retrieve error:", err)
		return
	}

	claims, err := decodeToken(token)
	if err != nil {
		return
	}

	render(c, "login-successfully.html", gin.H{
		"title": "Welcome, " + claims["login"].(string),
		"login": claims["login"],
	})
}

func signupUser(c *gin.Context) {
	login := c.PostForm("login")
	password := c.PostForm("password")

	if doesUserExist(login) {
		render(c, "signup-page.html", gin.H{
			"title":            "Sign Up",
			"userAlreadyExist": true,
		})
	}

	token, err := createToken(login, password)
	if err != nil {
		return
	}

	c.SetCookie("session_token", token, 3600, "/", "localhost", false, true)
	c.Set("is_logged_in", true)

	u := createUser(c)

	cacheData.SetCacheUser(u.Login, u)

	c.Redirect(http.StatusMovedPermanently, "/u/logged")
}

func logoutUser(c *gin.Context) {
	c.SetCookie("session_token", "", -1, "", "", false, true)
	c.Redirect(http.StatusTemporaryRedirect, "/")
}

func showGoalsPage(c *gin.Context) {
	render(c, "goals-page.html", gin.H{
		"title": "Your goals",
	})
}

func showGoalCreationPage(c *gin.Context) {
	render(c, "goal-creation-page.html", gin.H{
		"title": "Your goals",
	})
}

func createGoal(c *gin.Context) {
	c.Redirect(http.StatusMovedPermanently, "/")
}
