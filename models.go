package main

import (
	"mellorysite/cache"
	"mellorysite/config"

	"github.com/gin-gonic/gin"
)

func isUserValid(login, password string) bool {
	u, ok := cacheData.GetCacheUser(login)
	if !ok {
		return false
	}

	if checkPasswordHash(password, u.Value.Password) && login == u.Value.Login {
		return true
	}

	return false
}

func doesUserExist(login string) bool {
	_, ok := cacheData.GetCacheUser(login)
	return ok
}

func createUser(c *gin.Context) cache.User {
	var err error
	u := cache.User{
		UserID:    len(cacheData.Items) + 1,
		Name:      c.PostForm("name"),
		Login:     c.PostForm("login"),
		Password:  c.PostForm("password"),
		Privilege: "user",
	}
	u.Password, err = hashPassword(u.Password)
	if err != nil {
		return cache.User{}
	}
	return u
}

func createAdmin() {
	var err error

	mock := config.AdminAccount
	mock.Password, err = hashPassword(mock.Password)
	if err != nil {
		return
	}
	cacheData.SetCacheUser(mock.Login, mock)
}
